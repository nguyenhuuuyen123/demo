<!DOCTYPE html>
<html lang="en">
<head>
  <title>Quản lý sinh viên</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
  <h1>Bảng quản lý sinh viên</h1>
  <table class="table table-hover">
    <a href="update.php"><button type="button" class="btn btn-danger">Thêm</button></a>
    <thead>
      <tr>
        <th>Mã SV</th>
        <th>Họ Tên SV</th>
        <th>Địa chỉ</th>
        <th>SĐT</th>
        <th>Email</th>
      </tr>
      
    </thead>
    
  </table> 
  <?php
  include("connect.php");

  $data = '<table class="table">
        <tr>
          <th>msv</th>
          <th>hoten</th>
          <th>diachi</th>
          <th>sdt</th>
          <th>email</th>
          <th>Function</th>
        </tr>';
    $query = "SELECT * From sinhvien";

    if(!$result = mysqli_query ($con, $query)) 
    {
        exit(mysqli_error($con));
    }
    if(mysqli_num_rows($result) > 0)
    {
      while($row = mysqli_fetch_assoc($result))
      {
        $data .= '<tr>
          <td>'.$row['msv'].'</td>
          <td>'.$row['hoten'].'</td>
          <td>'.$row['diachi'].'</td>
          <td>'.$row['sdt'].'</td>
          <td>'.$row['email'].'</td>
          <td>
              <button class="btn btn-success" style="width: 80px">UPDATE</button>
              <button class="btn btn-danger" style="width: 80px">DELETE</button>
          </td>
        </tr>';
            
      }
    }
    else
    {
        $data .= '<tr><td colspan="6">Records not found!</td></tr>';
    }
 
    $data .= '</table>';
 
    echo $data;

?>
</div>

</body>
</html>
