<!DOCTYPE html>
<html>
<head>
	<title>Update</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<h1>  <center> Thêm thông tin sinh viên </center></h1>
<br>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="msv">Mã sinh viên</label>  
  <div class="col-md-4">
  <input id="msv" name="msv" type="text" placeholder="Nhập mã sinh viên" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ten">Họ Tên SV</label>  
  <div class="col-md-4">
  <input id="ten" name="ten" type="text" placeholder="Nhập họ tên" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="diachi">Địa chỉ</label>  
  <div class="col-md-4">
  <input id="diachi" name="diachi" type="text" placeholder="Nhập địa chỉ" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sdt">SDT</label>  
  <div class="col-md-4">
  <input id="sdt" name="sdt" type="text" placeholder="Nhập SDT" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="update"></label>
  <div class="col-md-4">
    <button id="update" name="update" class="btn btn-primary">Thêm</button>
  </div>
</div>

</fieldset>
</form>

</body>
</html>